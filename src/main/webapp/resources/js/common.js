$(document).ready(function(){
	$(document).on('click', '[dropdown-toggle]', function(){
		var area = $(this).attr('dropdown-toggle');
		$('[dropdown-area="' + area + '"]').slideToggle();
	});
});

function sckLabelName(sck_label) {
	switch (parseInt(sck_label, 10)) {
		case 0:
			return '<label class="label0 bold">미확인</label>';
		case 1:
			return '<label class="label1 bold">재고 있음</label>';
		case 2:
			return '<label class="label2 bold">재고 없음</label>';
	}
}

function dateFormat(format, date) {
	var d = new Date(date);
	var weekName = ["일", "월", "화", "수", "목", "금", "토"];
	
	return format.replace(/(yyyy|yy|mm|dd|e|H|h|i|s|ap)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear(); // 년도(4자리)
            case "yy": return d.getYear(); // 년도 (2자리)
            case "mm": return ((d.getMonth()+1) < 10 ? '0'+(d.getMonth()+1) : (d.getMonth()+1)); // 월
            case "dd": return (d.getDate() < 10 ? '0'+d.getDate() : d.getDate()); // 일
            case "e": return weekName[d.getDay()]; // 요일
            case "H": return d.getHours(); // 시(0~23)
            case "h": return ((h = d.getHours() % 12) ? h : 12); // 시(0~12)
            case "i": return d.getMinutes(); // 분
            case "s": return d.getSeconds(); // 초
            case "ap": return d.getHours() < 12 ? "오전" : "오후"; // 오전, 오후
            default: return $1;
        }
    });
}

