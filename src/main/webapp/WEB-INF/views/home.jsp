<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/views/common.jspf" %>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- HEAD -->
	<%@include file="/WEB-INF/views/header.jspf" %>
	<!-- HEAD -->

	<div class="searchArea">
		<input type="text" class="form-control" placeholder="물품을 검색해 주세요." onKeyup="productSearch(this);"/>
		<ul class="list-group" id="product-list"></ul>
		
		<h5 class="bold mt10" id="product-name">물품을 검색해주세요.</h5>
		<div id="brand-list"></div>
	</div>
	<!-- <div class="dropdown-box">
		<div class="dropdown-buttons">
		  	<button type="button" class="btn btn-primary dropdown-toggle" dropdown-toggle="product-list">
		    	물품 선택
		  	</button>
		  	<div dropdown-area="product-list"></div>
		</div>
	  	<div class="dropdown-buttons">
		  	<button type="button" class="btn btn-primary dropdown-toggle" dropdown-toggle="store-list">
		    	매장 선택
		  	</button>
		  	<div dropdown-area="store-list"></div>
	  	</div>
	</div> -->
	  
	<div id="map"></div>
	
	<!-- Modal -->
	<div id="storeModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	     	<h4 class="modal-title" id="storeName"></h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	      	<p>주소 : <strong id="storeAddress"></strong></p>
	      	<p>전화번호 : <strong id="storePhone"></strong></p>
	      	<p>물품 : <strong id="productName"></strong></p>
	      	<table border="1">
	      		<colgroup>
	      			<col class="wp60">
	      			<col class="wp20">
	      			<col class="wp20">
	      		</colgroup>
	      		<thead>
	      			<tr>
		      			<th>내용</th>
		      			<th>재고상태</th>
		      			<th>등록날짜</th>
		      		</tr>
	      		</thead>
	      		<tbody id="stockList">
	      			<tr>
	      				<td colspan="3">미확인 상태입니다.</td>
	      			</tr>
	      		</tbody>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
	      </div>
	    </div>
	  </div>
	</div>
</body>
<script>
var selectPrdId = null;
var map = null;
var geocoder = new kakao.maps.services.Geocoder();
$(document).ready(function(){
	document.getElementById('map').style.height = (window.innerHeight - 50) + 'px';
	if (navigator.geolocation) { // GPS를 지원하면
		navigator.geolocation.getCurrentPosition(function(position) {
			var latitude = position.coords.latitude;
			var longitude = position.coords.longitude;
			var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
			var options = { //지도를 생성할 때 필요한 기본 옵션
				center: new kakao.maps.LatLng(latitude, longitude), //지도의 중심좌표.
				level: 7 //지도의 레벨(확대, 축소 정도)
			};
			map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴
			
			var content = 
	 			'<div class="store myLocation">' +
	 		    '	<a href="javascript:;">내 위치</a>' +
	 		    '</div>';

	 		// 커스텀 오버레이가 표시될 위치입니다 
	 		var position = new kakao.maps.LatLng(latitude, longitude);  

	 		// 커스텀 오버레이를 생성합니다
	 		var customOverlay = new kakao.maps.CustomOverlay({
	 		    map: map,
	 		    position: position,
	 		    content: content,
	 		    yAnchor: 1 
	 		});
		}, 
		function(error) {
			console.error(error);
		}, 
		{
			enableHighAccuracy: false,
			maximumAge: 0,
			timeout: Infinity
		});
	} else {
		alert("GPS를 지원하지 않습니다.");
	}
});

function productSearch(_this) {
	var prd_name = $.trim(_this.value);
	if (prd_name === "") {
		$("#product-list").hide();
		return false;
	}
	$.ajax({
	    url: "/productSearch",
	    type: "GET",
	    data: { prd_name: prd_name },
	    dataType: "json",
	    success: function (resp) {
	   	 	if (parseInt(resp.resultCode) === 200) {
	   	 		var html = ''; 
	   	 		if (resp.productList.length > 0) {
		   	 		resp.productList.map((item, idx) => {
		   	 			html += '<li class="list-group-item" onClick="productSelect(' + item.prd_id + ')">' + item.prd_name + '</li>';
		   	 		});
		   	 		$("#product-list").html(html);
		   	 		$("#product-list").show();
	   	 		} else {
		   	 		$("#product-list").html(html);
		   	 		$("#product-list").hide();
	   	 		}
	   	 	}
	    }
	});
}

function productSelect(prd_id) {
	var brandCheckbox = $('.brandCheckbox[prd_id="' + prd_id + '"]');
	var brandArr = '';
	if (brandCheckbox.length > 0) {
		brandArr = [];
		$.each(brandCheckbox, function(index, item){
			if ($(this).prop('checked')) {
				brandArr.push($(this).val());
			}
		});
	}
	$('#product-list').hide();
	$.ajax({
	    url: "/productSelect",
	    type: "GET", 
	    data: { prd_id: prd_id, stb_id: JSON.stringify(brandArr) },
	    dataType: "json",
	    success: function (resp) {
	    	if (parseInt(resp.resultCode, 10) === 200) {
	    		selectPrdId = resp.productInfo.prd_id;
	    		var html = '';
	    		if (resp.brandList.length > 0) {
		    		resp.brandList.map((item, idx) => {
		    			var checked = brandArr ? (brandArr.indexOf(item.stb_id + '') !== -1 ? 'checked':'') : 'checked';
			    		html += '<div>';
			    		html += 	'<input type="checkbox" id="brand' + item.stb_id + '" class="brandCheckbox" prd_id="' + prd_id + '" value="' + item.stb_id + '" onclick="productSelect(' + selectPrdId + ')" ' + checked +'>';
			    		html += 	'<label for="brand' + item.stb_id + '"> ' + item.stb_name + '</label>';
			    		html += '</div>';
		    		});
	    		}
	    		$('#brand-list').html(html);
	    		$('#product-name').html(resp.productInfo.prd_name);
	    		$('.store.storeData').remove();
	    		resp.storeList.map((item, idx) => {
					// 주소로 좌표를 검색합니다
					geocoder.addressSearch(item.str_address1, function(result, status) {
					    // 정상적으로 검색이 완료됐으면 
					     if (status === kakao.maps.services.Status.OK) {
					 		var content = 
					 			'<div class="store storeData label' + item.sck_label + '">' +
					 		    '	<a href="javascript:storeStockInfo(' + item.str_id + ');">' +
					 		    		 item.str_name + 
					 		    '	</a>' +
					 		    '</div>';

					 		// 커스텀 오버레이가 표시될 위치입니다 
					 		var position = new kakao.maps.LatLng(result[0].y, result[0].x);  

					 		// 커스텀 오버레이를 생성합니다
					 		var customOverlay = new kakao.maps.CustomOverlay({
					 		    map: map,
					 		    position: position,
					 		    content: content,
					 		    yAnchor: 1 
					 		});
					     }
					});
				});	
	    	}
	    }
	});
}

function storeStockInfo(str_id)
{
	$.ajax({
	    url: "/storeStockInfo",
	    type: "GET", 
	    data: { str_id: str_id, prd_id: selectPrdId },
	    dataType: "json",
	    success: function (resp) {
	    	if (parseInt(resp.resultCode, 10) === 200) {
	    		$("#storeName").html(resp.storeInfo.str_name);
	    		$("#storeAddress").html(resp.storeInfo.str_address1);
	    		$("#storePhone").html(resp.storeInfo.str_phone);
	    		$("#productName").html(resp.productInfo.prd_name);
	    		var html = '';
	    		if (resp.stockList.length > 0) {
	    			resp.stockList.map(item => {
	    				html += '<tr>';
	    				html += 	'<td>' + item.sck_contents + '</td>';
	    				html += 	'<td>' + sckLabelName(item.sck_label) + '</td>';
	    				html += 	'<td>' + dateFormat('yyyy-mm-dd(e)<br/>ap h시 i분', item.sck_datetime) + '</td>';
	    				html += '</tr>';
	    			});
	    		} else {
	    			html += '<tr>';
	    			html += 	'<td colspan="3">미확인 상태입니다.</td>';
	    			html += '</tr>';
	    		}
	    		$("#stockList").html(html);
	    		$("#storeModal").modal();
	    	}
	    }
	});
}
</script>
</html>