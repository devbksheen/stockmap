<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/views/common.jspf" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>재고 품목들 관리</title>
	<style type="text/css">
		.modal-dialog {
			max-width: 850px;!important ;
		}
		.modal-dialog.modal-fullsize {
		  width: 100%;
		  height: 100%;
		  margin: 0;
		  padding: 0;
		}
		.modal-content.modal-fullsize {
		  height: auto;
		  min-height: 100%;
		  border-radius: 0; 
		}
		
		.modal.modal-center {
		  text-align: center;
		}
		
		@media screen and (min-width: 768px) { 
		  .modal.modal-center:before {
		  max-width:740px;
		    display: inline-block;
		    vertical-align: middle;
		    content: " ";
		    height: 100%;
		  }
		}
		
		.modal-dialog.modal-center {
		  display: inline-block;
		  text-align: left;
		  vertical-align: middle; 
		}
	</style>
</head>
<body>
	<!-- HEAD -->
	<%@include file="/WEB-INF/views/header.jspf" %>
	<!-- HEAD -->
	
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">한눈에 보기</button>
		<!-- 모달 영역 -->	
		<div class="modal fade modal-center" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">	
		  <div class="modal-dialog  modal-center" role="document">	
			  <div class="modal-content modal-fullsize" style="font-size: 12px;">	
				  <div class="modal-header">	
				  	<h4 class="modal-title" id="myModalLabel">항목</h4>	
				  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>	
				  </div>	
				  <div class="modal-body">
				       <ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#M">마켓</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#D">약국</a>
							</li>
						</ul>
						
						<div class="tab-content">
							<div class="tab-pane fade show active" id="M">
								<table class="table table-hover">
									<tr>
										<th>항목</th>
										<th>브랜드</th>
										<th>지점</th>
										<th>지점 연락처</th>
										<th>물품 이름</th>
										<th>물품 재고내용</th>
										<th>물품 재고상태 변경일</th>
									</tr>
									<c:forEach var="item" items="${stockMngMarketList}">
										<tr onclick="getModalView('${item}')">
											<td>
												<input type="checkbox" name="" />
											</td>
											<td>${item.stb_name}</td>
											<td>${item.str_name}</td>
											<td>${item.str_phone}</td>
											<td>${item.prd_name}</td>
											<td>${item.sck_contents}</td>
											<td>${item.sck_datetime}</td>
										</tr>
									</c:forEach>
								</table>
							</div>
				
							<div class="tab-pane fade" id="D">
								<table class="table table-hover">
									<tr>
										<th>항목</th>
										<th>브랜드</th>
										<th>지점</th>
										<th>지점 연락처</th>
										<th>물품 이름</th>
										<th>물품 재고내용</th>
										<th>물품 재고상태 변경일</th>
									</tr>
									<c:forEach var="item" items="${stockMngMedicalList}">
										<tr>
											<td>
												<input type="checkbox" name="" />
											</td>
											<td>${item.stb_name}</td>
											<td>${item.str_name}</td>
											<td>${item.str_phone}</td>
											<td>${item.prd_name}</td>
											<td>${item.sck_contents}</td>
											<td>${item.sck_datetime}</td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
				  </div>	
				  <div class="modal-footer">	
				  	<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>	
				  </div>	
			  </div>	
		  </div>	
		</div>

</body>

<script>
$(document).ready(function(){
	$.ajax({
	    url: "/testget",
	    type: "GET",
	    data: {},
	    dataType: "json",
	    success: function (resp) {
	    	console.log('resp', resp);
	    }
	});
});
</script>
</html>