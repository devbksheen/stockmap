<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/views/common.jspf" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>재고 등록</title>
</head>
<body>
	<!-- HEAD -->
	<%@include file="/WEB-INF/views/header.jspf" %>
	<!-- HEAD -->

	<div class="col-md-12 col-sm-12">
		<div class="col-md-10 col-sm-10 col-sm-offset-2" style="padding: 0px;">
			<form id="stockAddForm" action="${contextPath}/stockAddEditProc" method="post">
				<table style="margin-top: 33px;">
					<tr>
						<th>물품 : </th>
						<td>
							<select class="form-control" id="product" name="product">
								<option value="" selected="selected">물품을 선택해주세요.</option>
								<c:forEach var="item" items="${productList}">
									<option value="${item.prd_id}">${item.prd_name}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th>브랜드 : </th>
						<td id="brand-list">
							<select class="form-control" id="brand" name="brand">
								<option value="" selected="selected">브랜드를 선택해주세요.</option>
								<c:forEach var="item" items="${brandList}">
									<option value="${item.stb_id}">${item.stb_name}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th>지점 : </th>
						<td id="store-list">
							<select class="form-control">
								<option value="" selected="selected">브랜드를 먼저 선택해주세요.</option>
							</select>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th width="30%">재고여부 : </th>
						<td width="70%">
							<span style="width: 30%; background-color: red; border-radius: 5px;">
								재고없음 <input type="radio" checked="checked" name="sckLabel" class="" value="2"/>
							</span>
							&nbsp;&nbsp;
							<span style="width: 30%;  background-color: lime; border-radius: 5px;">
								재고있음 <input type="radio" name="sckLabel" class="" value="1"/>
							</span>
							&nbsp;&nbsp;
							<span style="width: 30%;  background-color: teal; border-radius: 5px;">
								파악안됨 <input type="radio" name="sckLabel" class="" value="0"/>
							</span>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr id="stockQuantity" style="display: none;">
						<th>재고수량 : </th>
						<td>
							<select name="stockQuantity" class="form-control">
								<option selected="selected">0 ~ 20개</option>
								<option>0 ~ 50개</option>
								<option>0 ~ 80개</option>
								<option>0 ~ 100개</option>
							</select>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th>재고에 대한 내용 : </th>
						<td>
							<textarea rows="10" cols="30" name="stockContent" class="form-control"></textarea>
						</td>
					</tr>
				</table>
				<div class="text-center" style="margin-top: 2%;">
					<button type="submit" class="btn btn-danger" style="width:100px;">확인</button>
					<button type="button" class="btn btn-secondary" style="width:100px; margin-left: 1%;" onclick="location.href='${contextPath}/'">취소</button>
				</div>
			</form>
		</div>
	</div>
</body>

<script>
$(document).ready(function(){
	$('#stockAddForm').bind('submit', function(e){
		let result = true;		
		if($.trim($('textarea[name="stockContent"]').val()) == ''){
			alert('재고에 대한 내용을 입력해주세요!!');
			result = false;
		}
		return result;	
	});

	$('input:radio[name="sckLabel"]').on('click', function(){
		(this.value && this.value == 1)? $('#stockQuantity').show() : $('#stockQuantity').hide();
	});
	
	//-- GetStoreList
	$('#brand').change(function(){
		if(this.value){
			$.ajax({
			    url: "/getBrandStoreList",
			    type: "GET",
			    data: { stb_id:this.value },
			    dataType: "json",
			    success: function (resp) {
		    		var html = '<select class="form-control" name="store">';
		    		$.each(resp.StoreList, function(idx, item){
		    			html += '<option class="dropdown-item" value="'+item.str_id+'">';
		    			html += 	item.str_name;
		    			html += '</option>';
		    		});
		    		html += '</select>';
		    		$("#store-list").html(html);
			    }
			})
		}
	});
});
</script>
</html>