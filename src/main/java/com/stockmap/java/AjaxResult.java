package com.stockmap.java;

import java.util.HashMap;

public class AjaxResult {
	
	public HashMap<String, Object> resultSuccess(HashMap<String, Object> data) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("resultCode", 200);
		for (String key : data.keySet()) {
			result.put(key, data.get(key));
		}
		return result;
	}
	
	public HashMap<String, Object> resultFail(int resultCode, String failMessage) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("resultCode", resultCode);
		result.put("failMessage", failMessage);
		return result;
	}
}
