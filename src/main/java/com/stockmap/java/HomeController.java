package com.stockmap.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stockmap.java.service.ProductService;
import com.stockmap.java.service.StockService;
import com.stockmap.java.service.StoreService;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private ProductService ProductService;
	@Autowired
	private StoreService StoreService;
	@Autowired
	private StockService StockService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "home";
	}
	
	@ResponseBody
	@RequestMapping(value = "/productSearch", method = RequestMethod.GET)
	public HashMap<String, Object> productSearch(@RequestParam String prd_name) throws IOException {
		// ��ǰ ���
		List<HashMap<String, Object>> productList = ProductService.productList(prd_name);
		
		HashMap<String, Object> resultData = new HashMap<String, Object>();
		resultData.put("productList", productList);

		AjaxResult AjaxResult = new AjaxResult();
		return AjaxResult.resultSuccess(resultData);
	}
	
	@ResponseBody
	@RequestMapping(value = "/productSelect", method = RequestMethod.GET)
	public HashMap<String, Object> productSelect(@RequestParam String stb_id, @RequestParam int prd_id) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		List<String> stbArr =  new ArrayList<String>();
		stb_id = stb_id.replaceAll("\"\"", "");
		if (!stb_id.equals("")) {
			try{ stbArr = mapper.readValue(stb_id, new TypeReference<List<String>>() {}); } catch (Exception e) {}
			if (stbArr.size() == 0) {
				stbArr.add("0");
			}
		}
		
		// ��ǰ�� ���� �귣�� ���
		List<HashMap<String, Object>> brandList = StoreService.productBrandList(prd_id);
		// ��ǰ ����
		HashMap<String, Object> productInfo = ProductService.productInfo(prd_id);
		// ���� ���
		List<HashMap<String, Object>> storeList = StoreService.productStoreList(stbArr, prd_id);

		HashMap<String, Object> resultData = new HashMap<String, Object>();
		resultData.put("brandList", brandList);
		resultData.put("productInfo", productInfo);
		resultData.put("storeList", storeList);

		AjaxResult AjaxResult = new AjaxResult();
		return AjaxResult.resultSuccess(resultData);
	}
	
	@ResponseBody
	@RequestMapping(value = "/storeStockInfo", method = RequestMethod.GET)
	public HashMap<String, Object> storeStockInfo(@RequestParam int str_id, @RequestParam int prd_id) throws IOException {
		// ���� ����
		HashMap<String, Object> storeInfo = StoreService.storeInfo(str_id);
		// ��ǰ ����
		HashMap<String, Object> productInfo = ProductService.productInfo(prd_id);
		// ��� ���
		List<HashMap<String, Object>> stockList = StockService.stockList(str_id, prd_id);

		HashMap<String, Object> resultData = new HashMap<String, Object>();
		resultData.put("storeInfo", storeInfo);
		resultData.put("productInfo", productInfo);
		resultData.put("stockList", stockList);

		AjaxResult AjaxResult = new AjaxResult();
		return AjaxResult.resultSuccess(resultData);
	}

}