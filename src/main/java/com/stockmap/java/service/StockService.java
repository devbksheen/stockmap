package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockService implements StockInterface {
	@Autowired
	private SqlSession sql;
	
	@Override
	public List<HashMap<String, Object>> stockList(int str_id, int prd_id) {
		HashMap<String, Integer> sqlParam = new HashMap<String, Integer>();
		sqlParam.put("str_id", str_id);
		sqlParam.put("prd_id", prd_id);
		return sql.selectList("stockList", sqlParam);
	}

	@Override
	public List<HashMap<String, Object>> getStoreBrandList(int prd_id) {
		return sql.selectList("getStoreBrandList", prd_id);
	}

	@Override
	public int stockAddEditProc(HashMap<String, Object> param) {
		int result = 0;
		int iSize  = 0;
		List<HashMap<String, Object>> m = sql.selectList("getRowStock", param);

		try {
			iSize = m.size();
			if(iSize > 0){
				result = sql.update("stockUpdateProc", param);
			} else {
				result = sql.insert("stockAddProc", param);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
	