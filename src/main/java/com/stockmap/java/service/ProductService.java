package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements ProductInterface {
	@Autowired
	private SqlSession sql;
	
	@Override
	public List<HashMap<String, Object>> productList(String prd_name) {
		return sql.selectList("productList", prd_name);
	}

	@Override
	public HashMap<String, Object> productInfo(int prd_id) {
		return sql.selectOne("productInfo", prd_id);
	}
}
