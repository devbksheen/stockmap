package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

public interface StockInterface {
	public List<HashMap<String, Object>> stockList(int str_id, int prd_id);
	public List<HashMap<String, Object>> getStoreBrandList(int prd_id);
	public int stockAddEditProc(HashMap<String,Object> param);
}
