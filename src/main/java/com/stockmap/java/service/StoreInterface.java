package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

public interface StoreInterface {
	public List<HashMap<String, Object>> productBrandList(int prd_id);
	public List<HashMap<String, Object>> productStoreList(List<String> stbArr, int prd_id);
	public HashMap<String, Object> storeInfo(int str_id);
	public List<HashMap<String, Object>> getBrandList();
	public List<HashMap<String, Object>> getBrandStoreList(HashMap<String, Object> param);
}