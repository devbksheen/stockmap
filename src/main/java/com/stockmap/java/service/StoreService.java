package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreService implements StoreInterface {
	@Autowired
	private SqlSession sql;
	
	public List<HashMap<String, Object>> productBrandList(int prd_id) {
		HashMap<String, Integer> sqlParam = new HashMap<String, Integer>();
		sqlParam.put("prd_id", prd_id);
		return sql.selectList("productBrandList", sqlParam);
	}
	
	public List<HashMap<String, Object>> productStoreList(List<String> stbArr, int prd_id) {
		HashMap<String, Object> sqlParam = new HashMap<String, Object>();
		sqlParam.put("stb_id", String.join(",", stbArr));
		sqlParam.put("prd_id", prd_id);
		return sql.selectList("productStoreList", sqlParam);
	}
	
	public HashMap<String, Object> storeInfo (int str_id) {
		HashMap<String, Object> sqlParam = new HashMap<String, Object>();
		sqlParam.put("str_id", str_id);
		return sql.selectOne("storeInfo", sqlParam);
	}

	@Override
	public List<HashMap<String, Object>> getBrandList() {
		return sql.selectList("getBrandList");
	}

	@Override
	public List<HashMap<String, Object>> getBrandStoreList(HashMap<String, Object> param) {
		return sql.selectList("getBrandStoreList", param);
	}

}
