package com.stockmap.java.service;

import java.util.HashMap;
import java.util.List;

public interface ProductInterface {
	public List<HashMap<String, Object>> productList(String prd_name);
	public HashMap<String, Object> productInfo(int prd_id);
}