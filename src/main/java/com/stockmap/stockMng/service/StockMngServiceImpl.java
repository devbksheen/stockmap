package com.stockmap.stockMng.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockMngServiceImpl implements StockMngService{

	@Autowired
	private SqlSession sql;
	
	@Override
	public List<HashMap<String, Object>> getStockMngList(HashMap<String, Object> param) {
		return sql.selectList("getStockMngList", param);
	}

}
