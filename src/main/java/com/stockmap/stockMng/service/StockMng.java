package com.stockmap.stockMng.service;

public class StockMng {
	
	/* COMMON */
	private String sckId;
	private String strId;
	private String prdId;
	private String stbId;
	
	/* SM_STOCK */
	private String sckContents;
	private String sckLabel;
	private String sckDatetime;
	private String sckCreated;
	
	/* SM_STORE */
	private String strName;
	private String strGubun;
	private String strPhone;
	private String strZipcode;
	private String strAddress1;
	private String strAddress2;
	
	/* SM_PRODUCCT */
	private String prdName;
	private String prdGubun;
	
	/* SM_STORE_BRAND */
	private String stbName;
	
	
	
	public String getStbId() {
		return stbId;
	}
	public void setStbId(String stbId) {
		this.stbId = stbId;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public String getStrGubun() {
		return strGubun;
	}
	public void setStrGubun(String strGubun) {
		this.strGubun = strGubun;
	}
	public String getStrPhone() {
		return strPhone;
	}
	public void setStrPhone(String strPhone) {
		this.strPhone = strPhone;
	}
	public String getStrZipcode() {
		return strZipcode;
	}
	public void setStrZipcode(String strZipcode) {
		this.strZipcode = strZipcode;
	}
	public String getStrAddress1() {
		return strAddress1;
	}
	public void setStrAddress1(String strAddress1) {
		this.strAddress1 = strAddress1;
	}
	public String getStrAddress2() {
		return strAddress2;
	}
	public void setStrAddress2(String strAddress2) {
		this.strAddress2 = strAddress2;
	}
	public String getPrdName() {
		return prdName;
	}
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	public String getPrdGubun() {
		return prdGubun;
	}
	public void setPrdGubun(String prdGubun) {
		this.prdGubun = prdGubun;
	}
	public String getStbName() {
		return stbName;
	}
	public void setStbName(String stbName) {
		this.stbName = stbName;
	}
	public String getSckId() {
		return sckId;
	}
	public void setSckId(String sckId) {
		this.sckId = sckId;
	}
	public String getStrId() {
		return strId;
	}
	public void setStrId(String strId) {
		this.strId = strId;
	}
	public String getPrdId() {
		return prdId;
	}
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	public String getSckContents() {
		return sckContents;
	}
	public void setSckContents(String sckContents) {
		this.sckContents = sckContents;
	}
	public String getSckLabel() {
		return sckLabel;
	}
	public void setSckLabel(String sckLabel) {
		this.sckLabel = sckLabel;
	}
	public String getSckDatetime() {
		return sckDatetime;
	}
	public void setSckDatetime(String sckDatetime) {
		this.sckDatetime = sckDatetime;
	}
	public String getSckCreated() {
		return sckCreated;
	}
	public void setSckCreated(String sckCreated) {
		this.sckCreated = sckCreated;
	}
	
}