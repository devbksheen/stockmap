package com.stockmap.stockMng.service;

import java.util.HashMap;
import java.util.List;

public interface StockMngService {
	public List<HashMap<String, Object>> getStockMngList(HashMap<String, Object> param);
}
