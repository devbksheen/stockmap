/**
 * Stock Management CLASS 
 */
package com.stockmap.stockMngController;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.stockmap.java.service.ProductService;
import com.stockmap.java.service.StockService;
import com.stockmap.java.service.StoreService;
import com.stockmap.stockMng.service.StockMngServiceImpl;

@Controller
public class StockMngController {
	
	@Autowired
	private ProductService ProductService;

	@Autowired
	private StoreService StoreService;

	@Autowired
	private StockService StockService;
	
	@Autowired
	private StockMngServiceImpl stockMngServiceImpl;

	
	@RequestMapping(value="/stockManagement")
	public ModelAndView stockManament(){
		HashMap<String, Object> m = new HashMap<String, Object>();
		ModelAndView mv = new ModelAndView();
		m.put("strGubun", "M");
		mv.setViewName("stockMng/stockManagement");
		mv.addObject("stockMngList", stockMngServiceImpl.getStockMngList(new HashMap<String, Object>()));
		mv.addObject("stockMngMarketList", stockMngServiceImpl.getStockMngList(m));
		m.put("strGubun", "D");
		mv.addObject("stockMngMedicalList", stockMngServiceImpl.getStockMngList(m));
		
		return mv;
	}
	
	@RequestMapping(value="/stockadd")
	public ModelAndView stockManamentAdd(){
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("brandList", StoreService.getBrandList());
			mv.addObject("productList", ProductService.productList(""));
			mv.setViewName("stockMng/stockMngAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mv;
	}
	
	@RequestMapping(value="/stockAddEditProc", method=RequestMethod.POST)
	public String stockManamentAddProc(@RequestParam HashMap<String, Object> param, HttpServletResponse res){
		System.out.println("param  : " +param);
		StockService.stockAddEditProc(param);
		return "redirect:/";
	}
	
	
	@RequestMapping(value="/getBrandStoreList")
	public ModelAndView getBrandStoreList(@RequestParam HashMap<String, Object> param){
		ModelAndView mv = new ModelAndView("jsonView");
		mv.addObject("StoreList",  StoreService.getBrandStoreList(param));

		return mv;
	}
	
}